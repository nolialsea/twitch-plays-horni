require('dotenv').config()
const axios = require('axios')
const conf = require('./conf.json')
const vocabIndexed = require('./vocabIndexed.json')
const http = require('http')
const fs = require('fs')
const {Server} = require("socket.io");


const storySteps = require('./scenario/longdong.json')

let step = 0
let story = ""
let choices = {}
let suggestions

let lastUpdate = Date.now()

function saveState() {
    const state = {
        step,
        story,
        choices,
        suggestions,
        lastUpdate
    }

    fs.writeFile("./state.json", JSON.stringify(state), () => {
    })
}

function loadState() {
    try {
        const state = require('./state.json')
        step = state.step
        story = state.story
        choices = state.choices
        suggestions = state.suggestions
        lastUpdate = state.lastUpdate
    } catch (e) {

    }
}

loadState()

const server = http.createServer((req, res) => {
        if (req.url === "/") {
            res.writeHead(200, {'content-type': 'text/html'})
            fs.createReadStream('twitchPlaysHorni.html').pipe(res)
        } else if (req.url === "/history") {
            sendHistoryFolder(res)
        } else if (req.url.startsWith("/history/")) {
            const path = req.url.replace("/history/", "")
            if (!path) {
                sendHistoryFolder(res, 404)
            } else {
                let file
                try {
                    file = fs.readFileSync("./archive/" + path)
                    res.writeHead(200, {'content-type': 'text/html'})
                    res.write(file.toString().replace(/\n/g, "<br/>"))
                    res.end()
                } catch (e) {
                    sendHistoryFolder(res, 404)
                }
            }
        }
    }
)

function sendHistoryFolder(res, code = 200) {
    res.writeHead(code, {'content-type': 'text/html'})
    const files = fs.readdirSync("./archive").reverse()
    res.write("<ul>")
    for (const file of files) {
        res.write("<li><a href='/history/" + file + "'>" + file + "</a></li>")
    }
    res.write("</ul>")
    res.end()
}

const io = new Server(server);


async function main() {
    if (storySteps[0].suggestions) {
        suggestions = [...storySteps[0].suggestions]
    } else {
        suggestions = await getSuggestions()
    }

    async function mainLoop() {
        if (choices) {
            const counts = countChoices()

            //If there are votes
            if (counts.reduce((a, b) => a + b, 0)) {
                const best = counts.indexOf(Math.max(...counts));
                if (best >= 0 && best < 4) {         // If suggestion
                    story += " " + suggestions[best]
                    story = format(story)
                    step++
                }

                if (best === 6) {
                    story += "\n"
                }

                const storyTokenLength = (await getTokens(story)).length
                if (best === 5 || storyTokenLength >= 2048) {
                    resetStory()
                }

                suggestions = await getSuggestions(best === 4)

                io.emit('story', story, storyTokenLength)
                io.emit('suggestions', suggestions)
            }

            io.emit('counterReset')
            choices = {}
        }

        saveState()

        lastUpdate = Date.now()
        setTimeout(mainLoop, 30 * 1000)
    }

    setInterval(() => {
        io.emit('choicesCount', countChoices())
        io.emit('suggestions', suggestions)

    }, 1000)

    await mainLoop()
}

function getDateFormatted() {
    const today = new Date();
    const hh = today.getHours().toString().padStart(2, "0")
    const min = today.getMinutes().toString().padStart(2, "0")
    const sec = today.getSeconds().toString().padStart(2, "0")
    const dd = today.getDate().toString().padStart(2, "0")
    const mm = (today.getMonth() + 1).toString().padStart(2, "0")
    const yyyy = today.getFullYear()
    return `${yyyy}-${mm}-${dd}_${hh}-${min}-${sec}`
}

function resetStory() {
    const storyLines = story.split("\n")
    if (storyLines.length >= 2) {
        const storyTitle = `${storyLines[0]}`
            .replace(/[ .,;"':]/g, "_")
        // Reset story
        fs.writeFile("archive/" + getDateFormatted() + "_" + storyTitle + '.txt', story, function (err) {
            if (err) return console.log(err);
        });
    }
    step = 0
    suggestions = null
    story = ""
}

async function getClampedPrompt() {
    const tokensPrompt = (await getTokens(story))
    return tokensPrompt
        .slice(-2048)
        .map((token) => vocabIndexed[token]
            .replace(/Ġ/g, ' ')
            .replace(/Ċ/g, '\n'))
        .join('')
}

async function getSuggestions(retry = false) {
    if (step < storySteps.length) {
        if (!retry && storySteps[step].text) {
            story += " " + storySteps[step].text
            story = format(story)
        }
        if (storySteps[step].suggestions) {
            return storySteps[step].suggestions
        } else if (storySteps[step].nbToken) {
            const answers = await getPromptAsync(await getClampedPrompt(), storySteps[step].nbToken ? storySteps[step].nbToken : 20, 0.9, 4, 90, 0.6, 2.5)
            if (storySteps[step].cropAfter) {
                return answers.map((a) => {
                    if (a.match(storySteps[step].cropAfter)) {
                        return a.split(storySteps[step].cropAfter)[0] + storySteps[step].cropAfter
                    }
                    return a + storySteps[step].cropAfter
                })
            } else {
                return answers.map(formatSuggestion)
            }
        } else {
            return ["Huh oh, error", "Huh oh, error", "Huh oh, error", "Huh oh, error"]
        }
    } else {
        return (await getPromptAsync(await getClampedPrompt(), 20, 0.9, 4, 90, 0.6, 2.5))
            .map(formatSuggestion)
    }
}

function formatSuggestion(sugg) {

    const reversedSugg = sugg.split('\n')[0]    // Keep only first line
        .split("").reverse().join("")           // Reverse

    const lastDelimiterIndex = reversedSugg.search(/[?!."\]]/)
    if (lastDelimiterIndex !== -1) {
        const clampedReversedSugg = reversedSugg.substr(lastDelimiterIndex)
        return clampedReversedSugg.split("").reverse().join("")
    } else {
        return reversedSugg.split("").reverse().join("")
    }
}

function format(str) {
    const formatted = str
        .replace(/([a-zA-Z0-9]) {2}([a-zA-Z0-9])/g, "$1 $2")
        .replace(/\n /g, "\n")
        .replace(/ \./g, ".")
        .replace(/ ,/g, ",")
        .replace(/' /g, "'")
        .replace(/ '/g, "'")
        .replace(/] \[/g, ']\n[')
        .replace(/] /g, ']\n')
        .replace(/\n "/g, "\n\"")
        .replace(/\n" /g, "\n\"")
        .replace(/""/g, '" "')
        .replace(/\. "/g, '.\n"')
        .replace(/ " /g, ' "')
        .replace(/ :/g, ':')
        .replace(/" "/g, '"\n"')
        .replace(/([a-zA-Z]) \?/g, '$1?')
        .replace(/([a-zA-Z]) !/g, '$1!')

    const lines = formatted.split('\n')
    let acc = [lines[0]]
    for (let i = 1; i < lines.length; i++) {
        const prevLineEmpty = lines[i - 1].trim() === ""
        const currLineEmpty = lines[i].trim() === ""

        const currLineStartsWithQuote = lines[i].startsWith('"')
        const prevLineStartsWithQuote = lines[i - 1].startsWith('"')
        const nextLineStartsWithQuote = i === lines.length - 1 ? false : lines[i + 1].startsWith('"')


        // Remove double empty lines
        if (prevLineEmpty && currLineEmpty) {
            continue
        }

        // Remove lines between discussions
        if (currLineEmpty && prevLineStartsWithQuote && nextLineStartsWithQuote) {
            continue
        }

        // Add newline when necessary
        if (!prevLineEmpty && !currLineEmpty && !prevLineStartsWithQuote && currLineStartsWithQuote) {
            acc.push("")
        } else if (!prevLineEmpty && !currLineEmpty && prevLineStartsWithQuote && !currLineStartsWithQuote) {
            acc.push("")
        }

        acc.push(lines[i])
    }

    return acc.join('\n')
}

function countChoices() {
    const counts = [0, 0, 0, 0, 0, 0, 0]
    for (const [key, value] of Object.entries(choices)) {
        if (value === -1) {
            counts[4]++
        } else if (value === -2) {
            counts[5]++
        } else if (value === -3) {
            counts[6]++
        } else {
            counts[value]++
        }
    }
    return counts;
}

io.on('connection', async (socket) => {

    io.emit('story', story, (await getTokens(story)).length)
    socket.emit('suggestions', suggestions)
    socket.emit('timer', Math.floor(30 - (Date.now() - lastUpdate) / 1000))

    let choice = -1
    socket.on('upvote', (sentChoice) => {
        choice = sentChoice
        choices[socket.id] = choice
        io.emit('choicesCount', countChoices())
    });
    socket.on('reroll', () => {
        choice = -1
        choices[socket.id] = choice
        io.emit('choicesCount', countChoices())
    });
    socket.on('reset', () => {
        choice = -2
        choices[socket.id] = choice
        io.emit('choicesCount', countChoices())
    });
    socket.on('newline', () => {
        choice = -3
        choices[socket.id] = choice
        io.emit('choicesCount', countChoices())
    });

    socket.on('disconnect', () => {
    });
});

server.listen(process.env.PORT || 3000)
main()


/**
 * Returns an AI generated text continuing your prompt
 * Retries until fulfillment
 * @param prompt
 * @param nbToken
 * @param temperature
 * @param nbResult
 * @param top_k
 * @param top_p
 * @param repetitionPenalty
 * @param repetitionPenaltyRange
 * @param repetitionPenaltySlope
 */
function getPromptAsync(prompt, nbToken = 1, temperature = 0.65, nbResult = 1, top_k = 50, top_p = 0.5, repetitionPenalty = 1.5, repetitionPenaltyRange = 512, repetitionPenaltySlope = 3.33) {
    return new Promise((accept) => {
        getPrompt(prompt, nbToken, (answer) => {
            accept(answer)
        }, temperature, nbResult, top_k, top_p, repetitionPenalty, repetitionPenaltyRange, repetitionPenaltySlope)
    })
}

/**
 * Returns an AI generated text continuing your prompt
 * Retries until fulfillment
 * @param prompt
 * @param nbToken
 * @param callback
 * @param temperature
 * @param nbResult
 * @param top_k
 * @param top_p
 * @param repetitionPenalty
 * @param repetitionPenaltyRange
 * @param repetitionPenaltySlope
 * @param nbFail
 */
function getPrompt(prompt, nbToken = 1, callback = (answer) => {
}, temperature = 0.65, nbResult = 1, top_k = 50, top_p = 0.5, repetitionPenalty = 1.5, repetitionPenaltyRange = 512, repetitionPenaltySlope = 3.33, nbFail = 0) {
    // Tries to generate a message until it works
    sendPrompt(prompt, nbToken, (message, err) => {
        if (message && !err) {
            callback(message)
        } else if (nbFail < 3) {
            getPrompt(prompt, nbToken, callback, temperature, nbResult, top_k, top_p, repetitionPenalty, repetitionPenaltyRange, repetitionPenaltySlope, ++nbFail)
        }
    }, temperature, nbResult, top_k, top_p, repetitionPenalty, repetitionPenaltyRange, repetitionPenaltySlope)
}

/**
 * Tries to generate a message with the AI API
 * @param prompt to feed to the AI
 */
function getTokens(prompt) {
    return new Promise((accept, reject) => {
        const data = {
            prompt,
        }

        axios.post(conf.apiUrlToken, data)
            .then((result) => {
                const answer = result.data
                if (answer) {
                    accept(answer)
                } else {
                    reject()
                }
            })
            .catch((err) => {
                console.log(err)
                reject()
            })
    })
}

/**
 * Tries to generate a message with the AI API
 * @param prompt to feed to the AI
 * @param nbToken
 * @param callback (aiMessage, err) => null either aiMessage or err if the message was null or empty after processing
 * @param temperature
 * @param nbResult
 * @param top_k
 * @param top_p
 * @param repetitionPenalty
 * @param repetitionPenaltyRange
 * @param repetitionPenaltySlope
 */
async function sendPrompt(prompt, nbToken, callback = (aiMessage, err) => null, temperature = 0.65, nbResult = 1, top_k = 50, top_p = 0.5, repetitionPenalty = 1.5, repetitionPenaltyRange = 512, repetitionPenaltySlope = 3.33) {

    const data = {
        prompt,
        nb_answer: nbResult,
        number_generated_tokens: nbToken,
        temperature,
        top_k,
        top_p,
        repetition_penalty: repetitionPenalty,
        repetition_penalty_range: repetitionPenaltyRange,
        repetition_penalty_slope: repetitionPenaltySlope,
        prevent_square_brackets: true,
        prevent_angle_brackets: true,
        prevent_curly_brackets: true
    }

    axios.post(conf.apiUrl, data)
        .then((result) => {
            const answer = result.data
            if (answer) {
                callback(answer)
            } else {
                callback(null, true)
            }
        })
        .catch((err) => {
            console.log(err)
            callback(null, true)
        })
}
