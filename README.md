# Twitch Plays Horni

Proof of concept for a proposition of free multiplayer minigame for NovelAI  
Live version should be running [HERE](http://86.193.242.125:3000/), if it's not, my old 1080ti is probably getting some rest

# How to play

1. Click shit
2. Wait
3. Go back to step 1

# History

Each time players reset the story, it is saved and accessible trough `/history` url or [HERE](http://86.193.242.125:3000/history) for the live version  
Stories are also automatically reset and saved when they get bigger than 2048 tokens

# Customizable scenarios

Scenarios are written in JSON files and need some explaining, so here is the default scenario with some commentaries   
```
[
  {
    "text": "[Tags:",   // Text is appended to story the first time a step is played (not if using retry)
    "suggestions": ["fantasy", "horror", "scifi", "explicit"],  // Suggestions are optional and should contain 4 strings
    "nbToken": null,    // Ignored if there are suggestions
    "cropAfter": null   // Ignored if there are suggestions
  },
  {
    "text": ",",
    "suggestions": null,
    "nbToken": 20,      // If there is no suggestion, you have to specify the number of tokens to generate at this step for each suggestion
    "cropAfter": "]"    // If using token generation, you can specify a stop string, content after stop string will be removed
  },
  {
    "text": null,
    "suggestions": ["You", "Alice", "I", "Once upon a time"],
    "nbToken": null,
    "cropAfter": null
  }
]
```
When starting a scenario, each step is played one by one consecutively and can be retried  
Steps with predefined suggestions can be retried, but there is no real effect as the suggestion won't change


# Prerequisites

If you want to host it yourself, you'll have to download and install AI Dungeon Clover Edition 
**and the gtp-neo horni-ln model** (it's what works the best right now)  
Then, all you have to do is copy/paste the `ai_rest_server.py` file into AID Clover in the same folder as `play.py` and execute it to start the AI REST API  
**Caution**: needs to run on GPU

It will expose two REST endpoints:  
POST `http://localhost:5000/prompt` to send a prompt and generate an answer  
POST `http://localhost:5000/tokens` to get the tokens from a prompt

# Install & Run (requires AI API to run too)
`npm i` installs requirements  
`node ./twitchPlaysHorniServer.js` starts the server
